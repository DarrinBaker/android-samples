package com.example.a003012613.samplecode;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.a003012613.samplecode.models.User;

import java.util.ArrayList;

/**
 * Created by 003012613 on 10/23/2017.
 */

public class UserListAdapter extends ArrayAdapter {

    ArrayList<User> users;

    public UserListAdapter(Context context, View view, int layoutResourceId, int x, ArrayList<User> users) {
        super(context, layoutResourceId, x);
        this.users = users;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        View userView = super.getView(position, view, parent);
        TextView lblFirstName = (TextView) userView.findViewById(R.id.lblFirstName);
        CheckBox chkActive = (CheckBox) userView.findViewById(R.id.chkActive);

        com.example.a003012613.samplecode.models.User u = users.get(position);
        lblFirstName.setText(u.getFirstName());
        chkActive.setChecked(u.isActive());

        // listners to be hooked up later

        return userView;
    }

}
