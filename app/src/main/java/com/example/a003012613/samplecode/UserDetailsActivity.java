package com.example.a003012613.samplecode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.a003012613.samplecode.dataaccess.UserDataAccess;
import com.example.a003012613.samplecode.models.User;

import java.util.ArrayList;

import static android.R.attr.id;

public class UserDetailsActivity extends AppCompatActivity {

    public static final String USER_ID_EXTRA = "userid: ";

    User user;

    AppClass app;

    EditText txtName;
    EditText txtEmail;
    RadioGroup rdgFavMusic;
    CheckBox chkActive;

    Button btnUpdateUserInDB;
    Button btnDeleteUserFromDB;
    Button btnInsertUserInDB;
    Button btnClearForm;

    // In the save button I need to access the UserDataAccess methods for insertUser and updateUser
    UserDataSQLiteOpenHelper dbHelper;
    UserDataAccess da;
    ListView usersListView;
    ArrayList<User> users;
    ArrayAdapter<User> adapter;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_details);

        app = (AppClass) getApplication();
        dbHelper = new UserDataSQLiteOpenHelper(this);
        da = new UserDataAccess(dbHelper);

        // handles on the buttons
        btnInsertUserInDB = (Button) findViewById(R.id.btnInsertUserIntoDB);
        btnUpdateUserInDB = (Button) findViewById(R.id.btnUpdateUserInDB);
        btnDeleteUserFromDB = (Button) findViewById(R.id.btnDeleteUserFromDB);
        btnClearForm = (Button) findViewById(R.id.btnClearForm);

        // handles on the data
        txtName = (EditText) findViewById(R.id.txtName);
        txtEmail = (EditText) findViewById(R.id.txtEmail);

        // Creates reference to radio group and clears values.
        rdgFavMusic = (RadioGroup) findViewById(R.id.rdgFavMusic);
        rdgFavMusic.clearCheck();

        // creates checkbox
        chkActive = (CheckBox) findViewById(R.id.chkActive);

        // Validation
        if (!txtName.getText().toString().equals("")) {
            txtName.setError("Name is a requried field");
        }

        String emailCheck = txtEmail.getText().toString().trim();
        if (!TextUtils.isEmpty(emailCheck) && Patterns.EMAIL_ADDRESS.matcher(emailCheck).matches()) {
            txtEmail.setError("Email address is not valid");
        }


        // Insert contact information - Add contact
        btnInsertUserInDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user = formToObj();

                if (isValid(user)) {
                    da.insertUser(user);
                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                }
            }
        });


        // Update contact information - Update contact
        btnUpdateUserInDB.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {

                user = formToObj();

                if (isValid(user)) {
                    da.updateUser(user);
                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));

                }
            }
        });


        // Delete contact information - Delete contact
        btnDeleteUserFromDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (user != null) {
                    da.deleteUser(user);
                    startActivity(new Intent(UserDetailsActivity.this, UserListActivity.class));
                }
            }
        });


        // Clear the form
        btnClearForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearForm();
            }
        });

        Intent i = getIntent();
        long userId = i.getLongExtra(USER_ID_EXTRA, -1);
        if (userId >= 0) {
            // Toast.makeText(this, "Get User: " + userId, Toast.LENGTH_SHORT).show();
            user = AppClass.getUserById(userId);
            objToForm();
        }

    } // end of onCreate

    // Methods
    private User formToObj() {
        // User info is written to the form, then saved in the database
        String nameUD = txtName.getEditableText().toString();
        String emailUD = txtEmail.getEditableText().toString();

        User.Music musicUD = null;
        int selectedRadioButtonId = rdgFavMusic.getCheckedRadioButtonId();

        switch (selectedRadioButtonId) {
            case R.id.rdoCountry:
                musicUD = User.Music.COUNTRY;
                break;
            case R.id.rdoRap:
                musicUD = User.Music.RAP;
                break;
            case R.id.rdoJazz:
                musicUD = User.Music.JAZZ;
                break;
        }

        boolean activeUD = chkActive.isChecked();

        if (user != null) {
            user.setFirstName(nameUD);
            user.setEmail(emailUD);
            user.setFavoriteMusic(musicUD);
            user.setActive(activeUD);

            return user;
        } else {
            return new User(id, nameUD, emailUD, musicUD, activeUD);
        }

    }


    private void objToForm() {
        // If a user exists, get the info and put it onto the form
        if (user != null) {
            txtName.setText(user.getFirstName());
            txtEmail.setText(user.getEmail());
            chkActive.setChecked(user.isActive());

            switch (user.getFavoriteMusic()) {
                case COUNTRY:
                    rdgFavMusic.check(R.id.rdoCountry);
                    break;
                case JAZZ:
                    rdgFavMusic.check(R.id.rdoJazz);
                    break;
                case RAP:
                    rdgFavMusic.check(R.id.rdoRap);
                    break;
            }
        } else {
            Toast.makeText(UserDetailsActivity.this, "There is no user yet", Toast.LENGTH_LONG).show();
        }
    }


    public void clearForm() {
        txtName.setText("");
        txtEmail.setText("");
        rdgFavMusic.clearCheck();
        chkActive.setChecked(false);
    }

/*  // Validation methods (Name,email, music) - not used but
    public boolean validateName(EditText txtName){
        return txtName.length() > 0;
    }

    public boolean validateEmail(EditText txtEmail) {

        String email = txtEmail.getText().toString().trim();

        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
        //     String emailPattern = getString(R.string.email_pattern);
        // Another validation attempt - (email.matches(emailPattern))
    }

    public boolean validateMusic(RadioGroup rdgFavMusic){
        return rdgFavMusic.isSelected();
    }
*/


    private boolean isValid(User u) {
        boolean hasError = false;
        String msg = "";
        if (u.getFirstName() == null || u.getFirstName().equals("")) {
            msg += "It seems that you have forgotten to enter the user's name.\n";
            hasError = true;
        }
        if (u.getEmail() == null || u.getEmail().equals("")) {
            msg += "It seems that you have forgotten to enter the user's email.\n";
            hasError = true;
        }
        if (u.getFavoriteMusic() == null) {
            msg += "It seems you have not set the favorite music.\n";
            hasError = true;
        }
        if (!hasError)
            return true;
        else {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            return false;
        }
    }


} //end of UserDetailActivity class


