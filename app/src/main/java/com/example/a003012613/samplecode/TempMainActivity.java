package com.example.a003012613.samplecode;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static com.example.a003012613.samplecode.R.id.btnCoder;
import static com.example.a003012613.samplecode.R.id.btnLifeCycle;

public class TempMainActivity extends AppCompatActivity {

    Button btnImgSpinner;
    Button btnIntentSender;
    Button btnUserDetails;

    Button btnAdapters;
    Button btnLifeCycle;
    Button btnCoder;
    Button btnWebService;
    Button btnSQLite;
    Button btnDatabaseManger;

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int buttonResourceId = view.getId();
            switch (buttonResourceId) {
                case R.id.btnImgSpinner:
                    startActivity(new Intent(TempMainActivity.this, ImageSpinnerActivity.class));
                    break;
                case R.id.btnIntentSender:
                    startActivity(new Intent(TempMainActivity.this, IntentSenderActivity.class));
                    break;
                case R.id.btnUserList:
                    startActivity(new Intent(TempMainActivity.this, UserListActivity.class));
                    break;
                case R.id.btnAdapters:
                    startActivity(new Intent(TempMainActivity.this, Adapters.class));
                    break;
                case R.id.btnLifeCycle:
                    startActivity(new Intent(TempMainActivity.this, LifeCycleActivity.class));
                    break;
                case R.id.btnCoder:
                    startActivity(new Intent(TempMainActivity.this, CoderActivity.class));
                    break;
                case R.id.btnWebService:
                    startActivity(new Intent(TempMainActivity.this, WebServiceActivity.class));
                    break;
                case R.id.btnSQLite:
                    startActivity(new Intent(TempMainActivity.this, SQLiteActivity.class));
                    break;
                case R.id.btnDatabaseManager:
                    startActivity(new Intent(TempMainActivity.this, AndroidDatabaseManager.class));

            }

        }
    };


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temp_main);

        btnImgSpinner = (Button) findViewById(R.id.btnImgSpinner);
        btnIntentSender = (Button) findViewById(R.id.btnIntentSender);
        btnUserDetails = (Button) findViewById(R.id.btnUserList);
        btnAdapters = (Button) findViewById(R.id.btnAdapters);
        btnLifeCycle = (Button) findViewById(R.id.btnLifeCycle);
        btnCoder = (Button) findViewById(R.id.btnCoder);
        btnWebService = (Button) findViewById(R.id.btnWebService);
        btnSQLite = (Button) findViewById(R.id.btnSQLite);
        btnDatabaseManger = (Button) findViewById(R.id.btnDatabaseManager);


        btnImgSpinner.setOnClickListener(listener);
        btnIntentSender.setOnClickListener(listener);
        btnUserDetails.setOnClickListener(listener);
        btnAdapters.setOnClickListener(listener);
        btnLifeCycle.setOnClickListener(listener);
        btnCoder.setOnClickListener(listener);
        btnWebService.setOnClickListener(listener);
        btnSQLite.setOnClickListener(listener);
        btnDatabaseManger.setOnClickListener(listener);


    }
}
