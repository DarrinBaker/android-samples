package com.example.a003012613.samplecode;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.a003012613.samplecode.dataaccess.UserDataAccess;
import com.example.a003012613.samplecode.models.User;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Created by 003012613 on 10/23/2017.
 */

public class AppClass extends Application {

    public static final String TAG = "AppClass";
    public String someGlobalVariable = "Hello there!";
    public static ArrayList<User> users = new ArrayList<>();
    public static final String USERS_FILE = "users.dat";

    UserDataSQLiteOpenHelper dbHelper;
    UserDataAccess da;


    @Override
    public void onCreate() {
        super.onCreate();

        dbHelper = new UserDataSQLiteOpenHelper(this);
        da = new UserDataAccess(dbHelper);

        // We're getting the information from the database not reading it from a file
        // But I left this in so I can remember what a file reading statement looks like
        // users = readUsersFromFile(USERS_FILE);

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    public void writeUsersToFile(ArrayList<User> users, String filePath) {
        try {
            FileOutputStream fos = openFileOutput(filePath, Context.MODE_PRIVATE);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(users);
            oos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found");
        } catch (IOException e) {
            Log.e(TAG, "IO Error");
        } catch (Exception e) {
            Log.e(TAG, "General Error");
        }
    }

    public ArrayList<User> readUsersFromFile(String filePath) {

        ArrayList<User> users = new ArrayList<>(); // users will be null if there is a problem
        try {
            FileInputStream is = openFileInput(filePath);
            ObjectInputStream ois = new ObjectInputStream(is);
            users = (ArrayList<User>) ois.readObject();
            ois.close();
            is.close();
        } catch (FileNotFoundException e) {
            Log.e(TAG, "File Not Found");
        } catch (IOException e) {
            Log.e(TAG, "IO Error");
        } catch (Exception e) {
            Log.e(TAG, "General Error");
        }

        return users;
    }

    public static User getUserById(long id) {
        for (User u : users) {
            if (id == u.getId()) {
                return u;
            }
        }
        return null;
    }


}
