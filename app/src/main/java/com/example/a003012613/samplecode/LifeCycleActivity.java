package com.example.a003012613.samplecode;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.a003012613.samplecode.models.User;

public class LifeCycleActivity extends AppCompatActivity {

    public static final String TAG = "LifeCycleActivity";

    User user = new User();
    Button btnIntent;
    EditText editText;
    Button btnSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_life_cycle2);

        if (savedInstanceState != null) {
            user.setFirstName(savedInstanceState.getString("FIRST_NAME"));
        }

        Log.d(TAG, "onCreate().........." + user.getFirstName());

        btnIntent = (Button) findViewById(R.id.btnIntent);
        btnIntent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "http://www.google.com";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        btnSave = (Button) findViewById(R.id.btnUpdateUserInDB);
        editText = (EditText) findViewById(R.id.editText);


        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user.setFirstName(editText.getText().toString());
                Log.d(TAG, "User first name: " + user.getFirstName());
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState().....");
        outState.putString("FIRST_NAME", user.getFirstName());
    }
}
