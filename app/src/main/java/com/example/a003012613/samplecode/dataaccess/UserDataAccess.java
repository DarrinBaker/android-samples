package com.example.a003012613.samplecode.dataaccess;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.a003012613.samplecode.UserDataSQLiteOpenHelper;
import com.example.a003012613.samplecode.models.User;

import java.util.ArrayList;
import java.lang.Boolean;

import static com.example.a003012613.samplecode.AppClass.users;

/**
 * Created by 003012613 on 11/15/2017.
 */

public class UserDataAccess {

    public static final String TAG = "UserDataAccess";

    private UserDataSQLiteOpenHelper dbHelper;
    private SQLiteDatabase db;

    public static final String TABLE_NAME = "users";
    public static final String COLUMN_USER_ID = "_id";
    public static final String COLUMN_USER_NAME = "name";
    public static final String COLUMN_USER_EMAIL = "email";
    public static final String COLUMN_USER_FAVORITE_MUSIC = "music";
    public static final String COLUMN_USER_ACTIVE = "active";

    public static final String TABLE_CREATE = String.format("create table %s ( %s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s TEXT, %s TEXT, %s TEXT)",
            TABLE_NAME,
            COLUMN_USER_ID,
            COLUMN_USER_NAME,
            COLUMN_USER_EMAIL,
            COLUMN_USER_FAVORITE_MUSIC,
            COLUMN_USER_ACTIVE);

    public UserDataAccess(UserDataSQLiteOpenHelper dbHelper) {
        this.dbHelper = dbHelper;
        this.db = this.dbHelper.getWritableDatabase();
    }

    public User insertUser(User u) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, String.valueOf(u.isActive()));

        long insertId = db.insert(TABLE_NAME, null, values);

        u.setId(insertId);
        return u;
    }

    public ArrayList<User> getAllUsers() {

        ArrayList<User> users = new ArrayList<>();
        String query = String.format("SELECT %s, %s, %s, %s, %s FROM %s", COLUMN_USER_ID, COLUMN_USER_NAME, COLUMN_USER_EMAIL,
                COLUMN_USER_FAVORITE_MUSIC, COLUMN_USER_ACTIVE, TABLE_NAME);

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        while (!c.isAfterLast()) {
            // Take the values from the database and create the user
            User u = new User();

            // ID, Name, email, are strings, easy to assign to the User
            u.setId(c.getLong(0));
            u.setFirstName(c.getString(1));
            u.setEmail(c.getString(2));

            // Favorite music is an enum, so this is a workaround
            String musicStr = c.getString(3);
            User.Music um = null;

            if (musicStr.equals("JAZZ")) {
                um = User.Music.JAZZ;
            } else if (musicStr.equals("COUNTRY")) {
                um = User.Music.COUNTRY;
            } else if (musicStr.equals("RAP")) {
                um = User.Music.RAP;
            }

            u.setFavoriteMusic(um);

            // Active is a boolean, this is the workaround
            String activeStr = c.getString(4);
            Boolean b = null;

            if (activeStr.equalsIgnoreCase("true")) {
                b = true;
            } else {
                b = false;
            }

            u.setActive(b);

            // Add the user to the ArrayList
            users.add(u);
            c.moveToNext();
        }
        c.close();
        return users;
    }


    public User updateUser(User u) {
        ContentValues values = new ContentValues();
        values.put(COLUMN_USER_ID, u.getId());
        values.put(COLUMN_USER_NAME, u.getFirstName());
        values.put(COLUMN_USER_EMAIL, u.getEmail());
        values.put(COLUMN_USER_FAVORITE_MUSIC, u.getFavoriteMusic().toString());
        values.put(COLUMN_USER_ACTIVE, Boolean.toString(u.isActive()));

        int rowsUpdated = db.update(TABLE_NAME, values, "_id = " + u.getId(), null);
        return u;
    }

    public int deleteUser(User u) {
        int rowsDeleted = db.delete(TABLE_NAME, COLUMN_USER_ID + " = " + u.getId(), null);
        return rowsDeleted;
    }

}
