package com.example.a003012613.samplecode;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a003012613.samplecode.models.*;
import com.example.a003012613.samplecode.models.User;

import java.util.ArrayList;

public class Adapters extends AppCompatActivity {

    public static final String TAG = "Adapters";
    ListView listView;
    AppClass app;

    String[] names = {"Bob", "Sally", "Betty"};
    ArrayList<User> users = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adapters);

        app = (AppClass) getApplication();
        Toast.makeText(this, app.someGlobalVariable, Toast.LENGTH_SHORT).show();

        listView = (ListView) findViewById(R.id.listView);

        users.add(new User(1, "Bob", "bob@bob.com", User.Music.JAZZ, true));
        users.add(new User(2, "Sally", "x@x.com", User.Music.COUNTRY, true));
        users.add(new User(3, "Betty", "a@a.com", User.Music.RAP, true));

        //example1();  // uses array of strings as the data source
        //example2(); // uses arraylist of Users as the data source
        example1(); // create a custom view for each item in the users arraylist
    }


    public void example1() {
        //   ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, names);

        ArrayList<String> userStrings = new ArrayList<>();
        for (User u : users) {
            userStrings.add(u.getFirstName() + " " + u.getFavoriteMusic());
        }

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, userStrings);
        listView.setAdapter(adapter1);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                User selectedUser = users.get(position);
                Toast.makeText(Adapters.this, selectedUser.toString(), Toast.LENGTH_SHORT).show();

            }
        });

       /* listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(Adapters.this, "POS: " + position, Toast.LENGTH_SHORT).show();
                String selectedString = names[position];
            }
        });*/
    }

    public void example2() {
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, users);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                User selectedUser = users.get(position);
                Toast.makeText(Adapters.this, selectedUser.getFirstName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void example3() {
        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, users) {
            @Override
            public View getView(int position, View view, ViewGroup parent) {
                View itemView = super.getView(position, view, parent);

                ListView listView = (ListView) parent;
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    }
                });


                TextView lblFirstName = (TextView) itemView.findViewById(R.id.lblFirstName);
                CheckBox chkActive = (CheckBox) itemView.findViewById(R.id.chkActive);

                User u = users.get(position);
                lblFirstName.setText(u.getFirstName());
                chkActive.setChecked(u.isActive());

                chkActive.setTag(u);

                chkActive.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        CheckBox pressedCheckBox = (CheckBox) v;
                        User u = (User) pressedCheckBox.getTag();
                        u.setActive(pressedCheckBox.isChecked());
                        Toast.makeText(Adapters.this, u.toString(), Toast.LENGTH_SHORT).show();
                    }
                });

                // listners to be hooked up later

                return itemView;
            }

        };

        listView.setAdapter(adapter);
    }

}
