package com.example.a003012613.samplecode.models;

/**
 * Created by 003012613 on 11/13/2017.
 */

public class Item {
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Item(long id, String name){
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString(){
        return this.name + " (" + this.id + ")";
    }
}
