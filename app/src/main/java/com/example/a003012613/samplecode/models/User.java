package com.example.a003012613.samplecode.models;

import java.io.Serializable;

/**
 * Created by 003012613 on 9/18/2017.
 */

public class User implements Serializable {
    public enum Music {COUNTRY, RAP, JAZZ}

    ;


    private String firstName;
    private String email;
    private Music favoriteMusic;
    private boolean active;

    private long id;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User() {

    }


    public User(long id, String firstName, String email, Music favoriteMusic, boolean active) {
        this.id = id;
        this.firstName = firstName;
        this.email = email;
        this.favoriteMusic = favoriteMusic;
        this.active = active;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Music getFavoriteMusic() {
        return favoriteMusic;
    }

    public void setFavoriteMusic(Music favoriteMusic) {
        this.favoriteMusic = favoriteMusic;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        // ID is not shown because the user shouldn't see it
        return String.format("Name: %s Email: %s Favorite Music: %s Active: %b", id, firstName, email, favoriteMusic, active);

    }

}
