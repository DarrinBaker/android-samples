package com.example.a003012613.samplecode;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.a003012613.samplecode.dataaccess.UserDataAccess;
import com.example.a003012613.samplecode.models.User;

import java.util.ArrayList;

import static com.example.a003012613.samplecode.UserDetailsActivity.USER_ID_EXTRA;

public class UserListActivity extends AppCompatActivity {

    public static final String TAG = "UserListActivity";

    AppClass app;

    Button btnAddNewUser;

    UserDataSQLiteOpenHelper dbHelper;
    UserDataAccess da;
    ListView userListView;
    ArrayList<User> users;

    ArrayAdapter<User> adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        app = (AppClass) getApplication();
        dbHelper = new UserDataSQLiteOpenHelper(this);
        da = new UserDataAccess(dbHelper);
        app.users = da.getAllUsers();

        userListView = (ListView) (findViewById(R.id.userListView));
        userListView = (ListView) (findViewById(R.id.userListView));
        btnAddNewUser = (Button) (findViewById(R.id.btnAddNewUser));

        //      Toast.makeText(this, users.toString(),Toast.LENGTH_LONG).show();

        btnAddNewUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(UserListActivity.this, UserDetailsActivity.class));
            }
        });


        ArrayAdapter<User> adapter = new ArrayAdapter<User>(this, R.layout.custom_user_list_item, R.id.lblFirstName, app.users) {


            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View userListView = super.getView(position, convertView, parent);

                User currentUser;
                currentUser = app.users.get(position);
                userListView.setTag(currentUser);
                // binds the user to the gui

                //
                TextView lbl = (TextView) userListView.findViewById(R.id.lblFirstName);
                CheckBox chk = (CheckBox) userListView.findViewById(R.id.chkActive);

                lbl.setText(currentUser.getFirstName());
                chk.setChecked(currentUser.isActive());
                chk.setText("Active");
                chk.setTag(currentUser);

                chk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User selectedUser = (User) v.getTag();
                        //  CheckBox c = (CheckBox)v;
                        //  selectedUser.setActive(c.isChecked());
                        selectedUser.setActive(((CheckBox) v).isChecked());
                    }
                });

                userListView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        User selectedUser = (User) v.getTag();

                        Intent i = new Intent(UserListActivity.this, UserDetailsActivity.class);
                        i.putExtra(USER_ID_EXTRA, selectedUser.getId());
                        startActivity(i);
                    }
                });


                return userListView;
            }


        };
        userListView.setAdapter(adapter);
    }


    public static class LifeCycleActivity extends AppCompatActivity {

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_life_cycle2);
        }
    }

   /* public void getAllUsers(){
        usersListView = (ListView)(findViewById(R.id.userListView));
        dbHelper = new UserDataSQLiteOpenHelper();
        da = new UserDataAccess(dbHelper);
        users = da.getAllUsers();
        adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1);
        usersListView.setAdapter(adapter);
    }*/
}
