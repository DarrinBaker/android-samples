package com.example.a003012613.samplecode;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class IntentSenderActivity extends AppCompatActivity {

    Button btnSend;
    EditText txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intent_sender);
        btnSend = (Button) findViewById(R.id.btnSend);
        txtMessage = (EditText) findViewById(R.id.txtMessage);

        // Hook up the event handler - create the intent
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            /*
                String url = "http://www.google.com";
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                startActivity(intent);
             */

                String messageText = txtMessage.getText().toString();
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(IntentReceiverActivity.MY_TEXT_DATA, messageText);
                String chooserTitle = "Which app would you like to handle this intent...";
                Intent chosenIntent = Intent.createChooser(intent, chooserTitle);
                startActivity(chosenIntent);
            }
        });
    }
}
